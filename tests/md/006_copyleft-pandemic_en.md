# The Copyleft Pandemic

It seems that we needed a global pandemic for publishers to finally
give open access. I guess we should say… thanks?

In my opinion it was a good +++PR+++ maneuver, who doesn't like
companies when they do *good*? This pandemic has shown its capacity
to fortify public and private institutions, no matter how poorly
they have done their job and how these new policies are normalizing
surveillance. But who cares, I can barely make a living publishing
books and I have never been involved in government work.

An interesting side effect about this “kind” and *temporal* openness
is about authorship. One of the most relevant arguments in favor
of intellectual property (+++IP+++) is the defense of authors'
rights to make a living with their work. The utilitarian and
labor justifications of +++IP+++ are very clear in that sense.
For the former, +++IP+++ laws confer an incentive for cultural
production and, thus, for the so-called creation of wealth. For
the latter, author's “labour of his body, and the work of his
hands, we may say, are properly his.”

But also in personal-based justifications the author is a primordial
subject for +++IP+++ laws. Actually, this justification wouldn't
exist if the author didn't have an intimate and qualitatively
distinctive relationship with her own work. Without some metaphysics
or theological conceptions about cultural production, this special
relation is difficult to prove---but that is another story.

![Locke and Hegel drinking tea while discussing several topics on Nothingland…](../img/p006_i001_en.jpg)

From copyfight, copyleft and copyfarleft movements, a lot of
people have argued that this argument hides the fact that most
authors can't make a living, whereas publishers and distributors
profit a lot. Some critics claim governments should give more
power to “creators” instead of allowing “reproducers” to do whatever
they want. I am not a fan of this way of doing things because
I don't think anyone should have more power---including authors---but
than to distribute, and also because in my world government is
synonymous with corruption and death. But diversity of opinions
is important, I just hope not all governments are like that.

So between copyright, copyfight, copyleft and copyfarleft defenders
there is usually a mysterious assent about producer relevance.
The disagreement comes with how this overview about cultural
production is or should translate into policies, legislation
and political organization.

In times of emergency and crisis we are seeing how easily it
is to “pause” those discussions and laws---or fast track [other
ones](https://www.theguardian.com/technology/2020/mar/06/us-internet-bill-seen-as-opening-shot-against-end-to-end-encryption).
On the side of governments this again shows how copyright and
authors' rights aren't natural laws nor are they grounded beyond
our political and economic systems. From the side of copyright
defenders, this phenomena makes it clear that authorship is an
argument that doesn't rely on the actual producers, cultural
phenomena or world issues… And it also shows that there are [librarians](https://blog.archive.org/2020/03/30/internet-archive-responds-why-we-released-the-national-emergency-library)
and [researchers](https://www.latimes.com/business/story/2020-03-03/covid-19-open-science)
fighting in favor of public interests; +++AKA+++, how important
libraries and open access are today and how they can't be replaced
by (online) bookstores or subscription-based research.

I find it very pretentious that [some authors](https://www.authorsguild.org/industry-advocacy/internet-archives-uncontrolled-digital-lending)
and [some publishers](https://publishers.org/news/comment-from-aap-president-and-ceo-maria-pallante-on-the-internet-archives-national-emergency-library)
didn't agree with this *temporal* openness of their work. But
let's not miss the point: this global pandemic has shown how
easily it is for publishers and distributors to opt for openness
or paywalls---who cares about the authors?… So next time you
defend copyright as authors' rights to make a living, think twice,
only few have been able to earn a livelihood, and while you think
you are helping them, you are actually making third parties richer.

In the end the copyright holders are not the only ones who defend
their interests by addressing the importance of people---in their
case the authors, but more generally and secularly the producers.
The copyleft holders---a kind of “cool” copyright holder that
hacked copyright laws---also defends their interest in a similar
way, but instead of authors, they talk about users and instead
of profits, they supposedly defend freedom.

There is a huge difference between each of them, but I just want
to denote how they talk about people in order to defend their
interests. I wouldn't put them in the same sack if it wasn't
because of these two issues.

Some copyleft holders were so annoying in defending Stallman.
*Dudes*, at least from here we don't reduce the free software
movement to one person, no matter if he's the founder or how
smart or important he is or was. Criticizing his actions wasn't
synonymous with throwing away what this movement has done---what
we have done!---, as a lot of you tried to mitigate the issue:
“Oh, but he is not the movement, we shouldn't have made a big
issue about that.” His and your attitude is the fucking issue.
Together you have made it very clear how narrow both views are.
Stallman fucked it up and was behaving very immaturely by thinking
the movement is or was thanks to him---we also have our own stories
about his behavior---, why don't we just accept that?

But I don't really care about him. For me and the people I work
with, the free software movement is a wildcard that joins efforts
related to technology, politics and culture for better worlds.
Nevertheless, the +++FSF+++, the +++OSI+++, +++CC+++, and other
big copyleft institutions don't seem to realize that a plurality
of worlds implies a diversity of conceptions about freedom. And
even worse, they have made a very common mistake when we talk
about freedom: they forgot that “freedom wants to be free.”

Instead, they have tried to give formal definitions of software
freedom. Don't get me wrong, definitions are a good way to plan
and understand a phenomenon. But besides its formality, it is
problematic to bind others to your own definitions, mainly when
you say the movement is about and for them.

Among all concepts, freedom is actually very tricky to define.
How can you delimit an idea in a definition when the concept
itself claims the inability of, perhaps, any restraint? It is
not that freedom can't be defined---I am actually assuming a
definition of freedom---, but about how general and static it
could be. If the world changes, if people change, if the world
is actually an array of worlds and if people sometimes behave
one way or the other, of course the notion of freedom is gonna
vary.

With freedom's different meanings we could try to reduce its
diversity so it could be embedded in any context or we could
try something else. I dunno, maybe we could make software freedom
an interoperable concept that fits each of our worlds or we could
just stop trying to get a common principle.

The copyleft institutions I mentioned and many other companies
that are proud to support the copyleft movement tend to be blind
about this. I am talking from my experiences, my battles and
my struggles when I decided to use copyfarleft licenses in most
parts of my work. Instead of receiving support from institutional
representatives, I first received warnings: “That freedom you
are talking about isn't freedom.” Afterwards, when I sought infrastructure
support, I got refusals: “You are invited to use our code in
your server, but we can't provide you hosting because your licenses
aren't free.” Dawgs, if I could, I wouldn't look for your help
in the first place, duh.

Thanks to a lot of Latin American hackers and pirates, I am little
by little building my and our own infrastructure. But I know
this help is actually a privilege: for many years I couldn't
execute many projects or ideas only because I didn't have access
to the technology or tuition. And even worse, I wasn't able to
look to a wider and more complex horizon without all this learning.

(There is a pedagogical deficiency in the free software movement
that makes people think that writing documentation and praising
self-taught learning is enough. From my point of view, it is
more about the production of a self-image in how a hacker or
a pirate *should be*. Plus, it's fucking scary when you realize
how manly, hierarchical and meritocratic this movement could
be).

According to copyleft folks, my notion of software freedom isn't
free because copyfarleft licenses prevents *people* from using
software. This is a very common criticism of any copyfarleft
license. And it is also a very paradoxical one.

Between the free software movement and open source initiative,
there has been a disagreement about who ought to inherit the
same type of license, like the General Public License. For the
free software movement, this clause ensures that software will
always be free. According to the open source initiative, this
clause is actually a counter-freedom because it doesn't allow
people to decide which license to use and it also isn't very
attractive for enterprise entrepreneurship. Let's not forget
that the institutions of both sides agree that the market is
essential for technology development.

Free software supporters tend to vanish the discussion by declaring
that open source defenders don't understand the social implication
of this hereditary clause or that they have different interests
and ways to change technology development. So it's kind of paradoxical
that these folks see the anti-capitalist clause of copyfarleft
licenses as a counter-freedom. Or they don't understand its implications
nor perceive that copyfarleft doesn't talk about technology development
in its insolation, but in its relationship with politics, society
and economy.

I won't defend copyfarleft against those criticisms. First, I
don't think I should defend anything because I am not saying
everyone should grasp our notion of freedom. Second, I have a
strong opinion against the usual legal reductionism among this
debate. Third, I think we should focus on the ways we can work
together, instead of paying attention to what could divide us.
Finally, I don't think these criticisms are wrong, but incomplete:
the definition of software freedom has inherited the philosophical
problem of how we define and what the definition of freedom implies.

That doesn't mean I don't care about this discussion. Actually,
it's a topic I'm very familiar with. Copyright has locked me
out with paywalls for technology and knowledge access, while
copyleft has kept me away with “licensewalls” with the same effects.
So let's take a moment to see how free the freedom is that the
copyleft institutions are preaching.

According to *Open Source Software & The Department of Defense*
(+++DoD+++), The +++U.S. DoD+++ is one of the biggest consumers
of open source. To put it in perspective, all tactical vehicles
of the +++U.S.+++ Army employs at least one piece of open source
software in its programming. Other examples are *the use* of
Android to direct airstrikes or *the use* of Linux for the ground
stations that operates military drones like the Predator and
Reaper.

![Reaper drones incorrectly bombarding civilians in Afghanistan, Iraq, Pakistan, Syria and Yemen in order to deliver +++U.S. DoD+++ notion of freedom.](../img/p006_i002_en.png)

Before you argue that this is a problem about open source software
and not free software, you should check out the +++DoD+++ [+++FAQ+++
section](https://dodcio.defense.gov/Open-Source-Software-FAQ).
There, they define open source software as “software for which
the human-readable source code is available for use, study, re-use,
modification, enhancement, and re-distribution by the users of
that software.” Does that sound familiar? Of course!, they include
+++GPL+++ as an open software license and they even rule that
“an open source software license must also meet the +++GNU+++
Free Software Definition.”

This report was published in 2016 by the Center for a New American
Security (+++CNAS+++), a right-wing think tank which [mission
and agenda](https://www.cnas.org/mission) is “designed to shape
the choices of leaders in the +++U.S.+++ government, the private
sector, and society to advance +++U.S.+++ interests and strategy.”

I found this report after I read about how the +++U.S.+++ Army
[scrapped](https://www.timesofisrael.com/us-army-scraps-1b-iron-dome-project-after-israel-refuses-to-provide-key-codes)
one billion dollars for its “Iron Dome” after Israel refused
to share key codes. I found it interesting that even the so-called
most powerful army in the world was disabled by copyright laws---a
potential resource for asymmetric warfare. To my surprise, this
isn't an anomaly.

The intention of +++CNAS+++ report is to convince +++DoD+++ to
adopt more open source software because its “generally better
than their proprietary counterparts […] because they can *take
advantage* of the *brainpower* of larger teams, which leads to
faster innovation, higher quality, and superior security for
*a fraction of the cost*.” This report has its origins by the
“justifiably” concern “about the erosion of +++U.S.+++ military
technical superiority.”

Who would think that this could happen to free and open source
software (+++FOSS+++)? Well, all of us from this part of the
world have been saying that the type of freedom endorsed by many
copyleft institutions is too wide, counterproductive for its
own objectives and, of course, inapplicable for our context because
that liberal notion of software freedom relies on strong institutions
and the capacity of own property or capitalize knowledge. The
same ones which have been trying to explain that the economic
models they try to “teach” us don't work or we doubt them because
of their side effects. Crowdfunding isn't easy here because our
cultural production is heavily dependent on government aids and
policies, instead of the private or public sectors. And donations
aren't a good idea because of the hidden interests they could
have and the economic dependence they generate.

But I guess it has to burst their bubble in order to get the
point across. For example, the Epstein controversial donations
to +++MIT+++ Media Lab and his friendship with some folks of
+++CC+++; or the use of open source software by the +++U.S.+++
Immigration and Customs Enforcement. While for decades +++FOSS+++
has been a mechanism to facilitate the murder of “Global South”
citizens; a tool for Chinese labor exploitation denounced by
the anti-996 movement; a licensewall for technological and knowledge
access for people who can't afford infrastructure and the learning
it triggers, even though the code is “free” *to use*; or a police
of software freedom that denies Latin America and other regions
their right to self-determinate its freedom, its software policies
and its economic models.

Those copyleft institutions that care so much about “user freedoms”
actually haven't been explicit about how +++FOSS+++ is helping
shape a world where a lot of us don't fit in. It had to be right-wing
think tanks, the ones that declare the relevance of +++FOSS+++
for warfare, intelligence, security and authoritarian regimes,
while these institutions have been making many efforts in justifying
its way of understanding cultural production as a commodification
of its political capacity. They have shown that in their pursuit
of government and corporate adoption of +++FOSS+++, when it favors
their interests, they talk about “software user freedoms” but
actually refer to “freedom of use software,” no matter who the
user is or what it has been used for.

There is a sort of cognitive dissonance that influences many
copyleft supporters to treat others---those who just want some
aid---harshly by the argument over which license or product is
free or not. But in the meantime, they don't defy, and some of
them even embrace, the adoption of +++FOSS+++ for any kind of
corporation, it doesn't matter if it exploits its employees,
surveils its users, helps to undermine democratic institutions
or is part of a killing machine.

In my opinion, the term “use” is one of the key concepts that
dilutes political capacity of +++FOSS+++ into the aestheticization
of its activity. The spine of software freedom relies in its
four freedoms: the freedoms of *run*, *study*, *redistribute*
and *improve* the program. Even though Stallman, his followers,
the +++FSF+++, the +++OSI+++, +++CC+++ and so on always indicate
the relevance of “user freedoms,” these four freedoms aren't
directly related to users. Instead, they are four different use
cases.

The difference isn't a minor thing. A *use case* neutralizes
and reifies the subject of the action. In its dilution the interest
of the subject becomes irrelevant. The four freedoms don't ban
the use of a program for selfish, slayer or authoritarian uses.
Neither do they encourage them. By the romantic idea of a common
good, it is easy to think that the freedoms of run, study, redistribute
and improve a program are synonymous with a mechanism that improves
welfare and democracy. But because these four freedoms don't
relate to any user interest and instead talk about the interest
of using software and the adoption of an “open” cultural production,
it hides the fact that the freedom of use sometimes goes against
and uses subjects.

So the argument that copyfarleft denies people the use of software
only makes sense between two misconceptions. First, the personification
of institutions---like the ones that feed authoritarian regimes,
perpetuate labor exploitation or surveil its users---and their
policies that sometimes restrict freedom or access *to people*.
Second, the assumption that freedoms over software use cases
is equal to the freedom of its users.

Actually, if your “open” economic model requires software use
cases freedoms over users freedoms, we are far beyond the typical
discussions about cultural production. I find it very hard to
defend my support of freedom if my work enables some uses that
could go against others' freedoms. This is of course a freedom
dilemma related to the [paradox of tolerance](https://en.wikipedia.org/wiki/Paradox_of_tolerance).
But my main conflict is when copyleft supporters boast about
their defense of users freedoms while they micromanage others'
software freedom definitions and, in the meantime, they turn
their backs to the gray, dark or red areas of what is implicit
in the freedom they safeguard. Or they don't care about us or
their privileges don't allow them to have empathy.

Since the *+++GNU+++ Manifesto* the relevance of industry among
software developers is clear. I don't have a reply that could
calm them down. It is becoming more clear that technology isn't
just a broker that can be used or abused. Technology, or at least
its development, is a kind of political praxis. The inability
of legislation for law enforcement and the possibility of new
technologies to hold and help the *statu quo* express this political
capacity of information and communications technologies.

So as copyleft hacked copyright law, with copyfarleft we could
help disarticulate structural power or we could induce civil
disobedience. By prohibiting our work from being used by military,
police or oligarchic institutions, we could force them to stop
*taking advantage* and increase their maintenance costs. They
could even reach a point where they couldn't operate anymore
or at least they couldn't be as affective as our communities.

I know it sounds like a utopia because in practice we need the
effort of a lot of people involved in technology development.
But we already did it once: we used copyright law against itself
and we introduced a new model of workforce distribution and means
of production. We could again use copyright for our benefit,
but now against the structures of power that surveils, exploits
and kills people. These institutions need our “brainpower,” we
can try by refusing their *use*. Some explorations could be software
licenses that explicitly ban surveillance, exploitation or murder.

We could also make it difficult for them to thieve our technology
development and deny access to our communication networks. Nowadays
+++FOSS+++ distribution models have confused open economy with
gift economy. Another think tank---Centre of Economics and Foreign
Policy Studies---published a report---*Digital Open Source Intelligence
Security: A Primer*---where it states that open sources constitutes
“at least 90%” of all intelligence activities. That includes
our published open production and the open standards we develop
for transparency. It is why end-to-end encryption is important
and why we should extend its use instead of allowing governments
to ban it.

Copyleft could be a global pandemic if we don't go against its
incorporation inside virulent technologies of destruction. We
need more organization so that the software we are developing
is “free as in social freedom, not only as in free individual.”
